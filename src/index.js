import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';

const render = Component => {
  ReactDOM.render(
    <AppContainer>
      <Component />
    </AppContainer>,
    document.getElementById('react-root')
  );
}

render(RootContainer);

if (module.hot) {
  module.hot.accept('./App.js', () => {
    const App = require('./App.js').default;
    render(<App />, document.getElementById('react-root'));
  })
}
