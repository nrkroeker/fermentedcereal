import { createTheme } from '@material-ui/core';
import indigo from '@material-ui/core/colors/indigo';
import amber from '@material-ui/core/colors/indigo';

const theme = createTheme({
  palette: {
    primary: indigo,
    secondary: amber
  }
});

export default theme;