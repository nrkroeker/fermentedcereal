import React from 'react';
import { DevTools } from 'mobx-react-devtools';
import { ThemeProvider } from '@material-ui/core';
import { Route, Router, Switch } from 'react-router';
import theme from './theme';

class App extends React.Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <DevTools />
        <Router>
          <Switch>
            <Route path="/practice" component={Practice} />
          </Switch>
        </Router>
      </ThemeProvider>
    )
  }
}

export default App;